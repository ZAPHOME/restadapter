package handlers

import (
	"net/http"
	"fmt"
	"encoding/json"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators"
	"bitbucket.org/zaphome/sharedapi/adapters/actuators/states"
	"github.com/gorilla/mux"
	"strconv"
	"io/ioutil"
	"io"
)

type ActuatorListModel struct {
	Actuators []ActuatorModel `json:"actuators"`
}

type ActuatorModel struct {
	Id    int          `json:"id"`
	Name  string       `json:"name"`
	State states.State `json:"state"`
}

type StateModel states.State

func ActuatorListIndex(w http.ResponseWriter, r *http.Request) {
	response, err := messageBus.SendRequestToSourceAndWaitForResponse(
		actuators.BuildActuatorListRequest(adapter.Id, 0))
	if err != nil {
		logger.Error("Cannot load actuator list: %v", err)
		w.Write([]byte(fmt.Sprintf("Cannot load actuator list: %v", err)))
		return
	}

	if actuatorListResponse, ok := (*response).(*actuators.ActuatorListResponse); ok {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		actuatorList := ActuatorListModel{}
		for _, actuator := range actuatorListResponse.ActuatorList {
			actuatorModel := ActuatorModel{
				Id:    actuator.Id,
				Name:  actuator.Name,
				State: actuator.State,
			}
			actuatorList.Actuators = append(actuatorList.Actuators, actuatorModel)
		}
		json.NewEncoder(w).Encode(actuatorList)
	} else {
		errMsg := "Cannot load actuator list: 500 Internal server error"
		logger.Error(errMsg)
		w.Write([]byte(errMsg))
	}
}

func SwitchActuatorHandler(w http.ResponseWriter, r *http.Request) {
	// Get Adapter ID from the request vars
	vars := mux.Vars(r)
	actuatorId := 0
	if id, err := strconv.Atoi(vars["actuatorId"]); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusNotFound)
		logger.ErrorOnError(json.NewEncoder(w).Encode(
			jsonErr{Code: http.StatusNotFound, Text: "Not Found"},
		))
		return
	} else {
		actuatorId = id
	}
	// Get the actuator list
	response, err := messageBus.SendRequestToSourceAndWaitForResponse(
		actuators.BuildActuatorListRequest(adapter.Id, 0))
	if err != nil {
		logger.Error("Cannot load actuator list: %v", err)
		w.Write([]byte(fmt.Sprintf("Cannot load actuator list: %v", err)))
		return
	}

	if actuatorListResponse, ok := (*response).(*actuators.ActuatorListResponse); ok {
		// Search for the target actuator
		var actuator *actuators.Actuator
		for _, a := range actuatorListResponse.ActuatorList {
			if actuatorId == a.Id {
				actuator = &a
				break
			}
		}
		if actuator == nil {
			// No actuator found -> 404 error
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusNotFound)
			logger.ErrorOnError(json.NewEncoder(w).Encode(
				jsonErr{Code: http.StatusNotFound, Text: "Not Found"},
			))
			return
		}

		var stateModel StateModel
		body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
		logger.ErrorOnError(err)
		logger.ErrorOnError(r.Body.Close())
		if err := json.Unmarshal(body, &stateModel); err != nil {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(422) // unprocessable entity
			logger.ErrorOnError(json.NewEncoder(w).Encode(err))
			return
		}

		// Switch the actuator
		stateChangeInstruction := states.BuildActuatorStateChangeInstruction(
			adapter.Id, actuator.AdapterId, actuator.Id, stateModel)
		messageBus.SourceChannel.SendMessage(stateChangeInstruction)

		// Send OK response
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
	} else {
		// Actuator list could not be received -> 500 Int. server error
		errMsg := "Cannot load actuator list: 500 Internal server error"
		logger.Error(errMsg)
		w.Write([]byte(errMsg))
		w.WriteHeader(http.StatusInternalServerError)
	}
}
