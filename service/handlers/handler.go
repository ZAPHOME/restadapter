package handlers

import (
	"fmt"
	"html"
	"net/http"

	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
)

var (
	ctx        *context.Context
	adapter    *adapters.Adapter
	messageBus *communication.MessageBus
	logger     *logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	if l, ok := (*ctx.GetService("Logger")).(logging.Logger); ok {
		logger = &l
	}
	if a, ok := (*ctx.GetService("Adapter")).(adapters.Adapter); ok {
		adapter = &a
		messageBus = a.MessageBus
	}
}

func ApiIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
}

func OptionsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "content-type")
}
