package handlers

import (
	"net/http"
	"fmt"
	"encoding/json"
	"bitbucket.org/zaphome/sharedapi/adapters"
)

type AdapterListModel struct {
	Adapters []AdapterModel `json:"adapters"`
}

type AdapterModel struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func AdapterListIndex(w http.ResponseWriter, r *http.Request) {
	response, err := messageBus.SendRequestToSourceAndWaitForResponse(
		adapters.BuildAdapterListRequest(adapter.Id, 0))
	if err != nil {
		logger.Error("Cannot load adapter list: %v", err)
		w.Write([]byte(fmt.Sprintf("Cannot load adapter list: %v", err)))
		return
	}

	if adapterListResponse, ok := (*response).(*adapters.AdapterListResponse); ok {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		adapterList := AdapterListModel{}
		for _, adapter := range adapterListResponse.AdapterList {
			adapterModel := AdapterModel{
				Id:   adapter.Id,
				Name: adapter.Name,
			}
			adapterList.Adapters = append(adapterList.Adapters, adapterModel)
		}
		json.NewEncoder(w).Encode(adapterList)
	} else {
		errMsg := "Cannot load adapter list: 500 Internal server error"
		logger.Error(errMsg)
		w.Write([]byte(errMsg))
	}
}
