package service

import (
	"github.com/gorilla/mux"
	"net/http"
	"bitbucket.org/zaphome/restadapter/service/handlers"
)

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = baseHandler(handler, route.Method)
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(PATH_PREFIX + route.Pattern).
			Name(route.Name).
			Handler(handler)
		router.
			Methods("OPTIONS").
			Path(PATH_PREFIX + route.Pattern).
			Name(route.Name).
			Handler(http.HandlerFunc(handlers.OptionsHandler))
	}
	return router
}

func baseHandler(inner http.Handler, method string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Method", method)
		w.Header().Set("Access-Control-Allow-Headers", "content-type")
		inner.ServeHTTP(w, r)
	})
}
