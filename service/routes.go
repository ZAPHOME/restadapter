package service

import (
	"net/http"

	"bitbucket.org/zaphome/restadapter/service/handlers"
)

type Routes []Route

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

const PATH_PREFIX = "/api/v1/"

var routes = Routes{
	Route{
		"Index",
		"GET",
		"",
		handlers.ApiIndex,
	},
	Route{
		"AdapterIndex",
		"GET",
		"adapter",
		handlers.AdapterListIndex,
	},
	Route{
		"ActuatorIndex",
		"GET",
		"actuator",
		handlers.ActuatorListIndex,
	},
	Route{
		"SwitchActuator",
		"POST",
		"actuator/{actuatorId}/switch",
		handlers.SwitchActuatorHandler,
	},
}
