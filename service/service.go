package service

import (
	"net/http"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/restadapter/service/handlers"
)

var (
	ctx        *context.Context
	logger     *logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	if l, ok := (*ctx.GetService("Logger")).(logging.Logger); ok {
		logger = &l
	}
	handlers.Initialize(ctx)
	go listen()
}

func listen() {
	router := NewRouter()
	logger.ErrorOnError(http.ListenAndServe(":8080", router))
}
