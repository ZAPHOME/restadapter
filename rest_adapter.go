package main

import (
	"bitbucket.org/zaphome/adapterbase/configuration"
	"bitbucket.org/zaphome/restadapter/service"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/sharedapi/logging"
)

const AdapterName = "REST"

var (
	ctx    *context.Context
	logger logging.Logger
)

func Initialize(adapt adapters.Adapter) adapters.Adapter {
	adapt.Name = AdapterName

	ctx = configuration.Init(&adapt)
	logger = (*ctx.GetService("Logger")).(logging.Logger)

	// Initialize the REST service
	service.Initialize(ctx)
	return adapt
}
