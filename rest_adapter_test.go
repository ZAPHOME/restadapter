package main

import (
	"testing"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/messaging/communication"
)

func TestInitialize(t *testing.T) {
	Initialize(adapters.Adapter{
		Id:         0,
		Name:       "Test",
		MessageBus: communication.GetNewMessageBus(),
	})
}
